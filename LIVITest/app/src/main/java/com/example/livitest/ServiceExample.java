package com.example.livitest;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public abstract class ServiceExample extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Run();
        return START_NOT_STICKY;
    }

    abstract void Run();

    public static void Start(ServiceExample service) {
        Context context = App.getAppContext();
        Intent serviceIntent = new Intent(context, service.getClass());
        context.startService(serviceIntent);
    }
}
