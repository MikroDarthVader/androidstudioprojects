package com.example.livitest;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static App mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        /*IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(FsService.ACTION_STARTED);
        intentFilter.addAction(FsService.ACTION_STOPPED);
        intentFilter.addAction(FsService.ACTION_FAILEDTOSTART);

        registerReceiver(new NsdService.ServerActionsReceiver(), intentFilter);
        registerReceiver(new FsWidgetProvider(), intentFilter);*/
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }
}
