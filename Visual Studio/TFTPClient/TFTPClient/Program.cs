﻿using System.Diagnostics;
using Tftp.Net;
Console.ReadKey();

AutoResetEvent TransferFinishedEvent = new AutoResetEvent(false);

string filename = "clusterUpdate.iso";//"mount-fuji-panaromic-8k-uj.jpg";//"image_16.12.2021.iso";

var client = new TftpClient("192.168.40.1", 6969);
var transfer = client.Download(filename);
transfer.TransferMode = TftpTransferMode.octet;
transfer.BlockSize = 8196;

//Capture the events that may happen during the transfer
int progress_ = -1;
transfer.OnProgress += new TftpProgressHandler(transfer_OnProgress);
transfer.OnFinished += new TftpEventHandler(transfer_OnFinshed);
transfer.OnError += new TftpErrorHandler(transfer_OnError);

//Start the transfer and write the data that we're downloading into a memory stream
Stream stream = new FileStream(@"C:\Users\Y134078\Downloads\Android\AndroidStudioProjects\Visual Studio\" + filename, FileMode.OpenOrCreate);

Stopwatch stopWatch = new Stopwatch();
stopWatch.Start();
transfer.Start(stream);

//Wait for the transfer to finish
TransferFinishedEvent.WaitOne();
stopWatch.Stop();
TimeSpan ts = stopWatch.Elapsed;
string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
    ts.Hours, ts.Minutes, ts.Seconds,
    ts.Milliseconds / 10);
Console.WriteLine("RunTime " + elapsedTime);

Console.ReadKey();



void transfer_OnProgress(ITftpTransfer transfer, TftpTransferProgress progress)
{
    int perc = (int)((float)progress.TransferredBytes / progress.TotalBytes * 100);
    if (perc > progress_)
    {
        progress_ = perc;
        Console.WriteLine("Transfer running. Progress: " + progress);
    }
}

void transfer_OnError(ITftpTransfer transfer, TftpTransferError error)
{
    Console.WriteLine("Transfer failed: " + error);
    TransferFinishedEvent.Set();
}

void transfer_OnFinshed(ITftpTransfer transfer)
{
    Console.WriteLine("Transfer succeeded.");
    TransferFinishedEvent.Set();
}