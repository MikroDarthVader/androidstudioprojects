package com.avtovaz.clusterupdate.ui.main;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.avtovaz.clusterupdate.R;
import com.avtovaz.clusterupdate.UpdateApplication;

import java.util.Iterator;

public class UpdateViewModel extends AndroidViewModel {

    public UpdateViewModel(@NonNull Application application) {
        super(application);
    }

    MutableLiveData<Boolean> isError = new MutableLiveData<Boolean>(false);
    MutableLiveData<Boolean> isStarted = new MutableLiveData<Boolean>(false);
    MutableLiveData<Boolean> isFinished = new MutableLiveData<Boolean>(false);
    MutableLiveData<Boolean> isCanceled = new MutableLiveData<Boolean>(false);
    MutableLiveData<String> currentStatus = new MutableLiveData<String>("");

    private UpdateRepository repository = new UpdateRepository(getApplication());

    public MutableLiveData<Boolean> getIsError() {
        return isError;
    }

    public MutableLiveData<Boolean> getIsStarted() {
        return isStarted;
    }

    public MutableLiveData<Boolean> getIsFinished() {
        return isFinished;
    }

    public MutableLiveData<Boolean> getIsCanceled() {
        return isCanceled;
    }

    public MutableLiveData<String> getCurrentStatus() {
        return currentStatus;
    }

    public void startUpdate() throws InterruptedException {

        postStart();

        //currentStatus.postValue(getApplication().getString(R.string.clean_data_status));
        runSteps(repository.getSteps());
    }

    public void continueUpdate() throws InterruptedException {

        isError.postValue(false);
        isCanceled.postValue(false);
        isStarted.postValue(true);

        /*Iterator<UpdateStep> steps = repository.updateSteps.iterator();
        while (steps.hasNext()) {
            UpdateStep step = steps.next();
            if (step.name == currentStatus.getValue()) {
                runSteps(steps, step);
                break;
            }
        }*/

        startUpdate();
    }

    public void runSteps(Iterator<UpdateStep> steps) {
        UpdateStep current;
        if (steps.hasNext()) {
            current = steps.next();
        } else {
            postFinish();
            return;
        }

        currentStatus.postValue(current.name);

        Log.d("Update", "Start step: " + current.name);
        current.work.run(result -> {
            if (result == StepWork.COMPLETE) {
                Log.d("Update", "Step complete: " + current.name);
                runSteps(steps);
            } else if (result == StepWork.ERROR) {
                Log.d("Update", "Error on step: " + current.name);
                isError.postValue(true);
            } else if (result == StepWork.CANCELLED) {
                Log.d("Update", "Cancelled on step: " + current.name);
                //isCanceled.postValue(true);
            }
        });
    }

    public void postStart() {
        repository.Init(UpdateApplication.getExecutor());

        isError.postValue(false);
        isCanceled.postValue(false);
        isStarted.postValue(true);
    }

    public void postFinish() {
        isStarted.postValue(false);
        isFinished.postValue(true);
    }

    public void cancelUpdate() {
        repository.CancelWork();
        Log.d("ViewModel", "Cancellation event");
        isCanceled.postValue(true);
    }
}
