package com.avtovaz.clusterupdate.ftp.server;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.IOException;

public class FtpUser {

    final private String mUsername;
    final private String mPassword;
    final private String mChroot;

    public FtpUser(@NonNull String username, @NonNull String password, @NonNull String chroot) throws IOException {
        mUsername = username;
        mPassword = password;

        final File rootPath = new File(chroot);
        if(! rootPath.isDirectory())
            throw new IOException("chrootDir is not a directory");
        mChroot = chroot;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getChroot() {
        return mChroot;
    }
}
