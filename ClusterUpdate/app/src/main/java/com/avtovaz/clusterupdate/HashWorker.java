package com.avtovaz.clusterupdate;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicReference;

public class HashWorker {
    public String TAG = "HashWorker";

    public String CalculateHash(File forHash, AtomicReference<Boolean> cancelled) {

        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA256");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Exception while getting digest", e);
            return null;
        }

        InputStream is;
        try {
            is = new FileInputStream(forHash);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Exception while getting FileInputStream", e);
            return null;
        }

        byte[] buffer = new byte[65536];
        int read;
        try {
            while ((read = is.read(buffer)) > 0 && !cancelled.get()) {
                digest.update(buffer, 0, read);
            }
            byte[] sha256sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, sha256sum);
            String output = bigInt.toString(16);
            // Fill to 32 chars
            output = String.format("%32s", output).replace(' ', '0');
            return output;
        } catch (IOException e) {
            throw new RuntimeException("Unable to process file for SHA256", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(TAG, "Exception on closing SHA256 input stream", e);
            }
        }
    }
}

