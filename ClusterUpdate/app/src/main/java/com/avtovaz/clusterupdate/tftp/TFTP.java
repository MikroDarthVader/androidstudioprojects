package com.avtovaz.clusterupdate.tftp;

import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* TFTP server */
public class TFTP extends UDPServer {

    private static String TAG = Server.class.getSimpleName();

    /* Get the port that the server is bound to */
    @Override
    protected int getPort() {
        return TFTPConfig.tftp_server_port;
    }

    /* Get the address that the server is bound to */
    @Override
    protected InetAddress getInterfaceAddress() {
        InetAddress returnAddress = null;

        try {
            ArrayList<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : networkInterfaces) {
                if (!networkInterface.getName().matches(TFTPConfig.network_interface))
                    continue;
                for (InetAddress address : Collections.list(networkInterface.getInetAddresses())) {
                    if (!address.isLoopbackAddress()
                            && !address.isLinkLocalAddress()
                            && address instanceof Inet4Address) {
                        if (returnAddress != null) {
                            Log.w(TAG, "Found more than one valid address local inet address, why???");
                        }
                        returnAddress = address;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnAddress;
    }

    @Override
    protected void RunCoprocess() {
        TFTPSession.Manager.Prune();
    }

    /* Sends a TFTP packet */
    private void Send(InetSocketAddress Target, int Port, TFTPPacket rep) throws Exception {
        super.Send(Port, Target.getAddress(), rep.Encode());
    }

    /* Handle a RRQ */
    private TFTPPacket onReadRequest(TFTPPacket pkt, TFTPSession ses) throws Exception {
        /* Get target */
        String targetPath = Path.ParseDots(Path.Slashify(pkt.filename), false, false);
        /* Open the file */
        ses.openFile(ses.getRootPath() + targetPath, false);
        /* If packet has options, parse and acknowledge them */
        if (pkt.hasOptions())
            return onOptions(pkt, ses);
        /* Otherwise, start reading the file and send data back */
        TFTPPacket rep = TFTPPacket.DATA((char) 1, null);
        ses.initRRQ();
        ses.readFile(rep);
        return rep;
    }

    /* Handle options */
    private TFTPPacket onOptions(TFTPPacket pkt, TFTPSession ses) throws Exception {
        List<String> k = new ArrayList<String>();
        List<String> v = new ArrayList<String>();
        if (pkt.hasOption(TFTPPacket.OPTION_BLOCKSIZE)) {
            ses.setBlockSize(Integer.parseInt(pkt.getOption(TFTPPacket.OPTION_BLOCKSIZE)));
            k.add(TFTPPacket.OPTION_BLOCKSIZE);
            v.add(((Integer) ses.getBlockSize()).toString());
        }
        if (pkt.hasOption(TFTPPacket.OPTION_FILESIZE)) {
            k.add(TFTPPacket.OPTION_FILESIZE);
            v.add(((Long) ses.getFileSize()).toString());
        }
        return TFTPPacket.OACK(k.toArray(new String[]{}), v.toArray(new String[]{}));
    }

    /* Handle a WRQ */
    private TFTPPacket onWriteRequest(TFTPPacket pkt, TFTPSession ses) throws Exception {
        return TFTPPacket.ERROR(TFTPPacket.ERROR_ACCESS_VIOLATION, "Writing is not implemented on this server");
    }

    /* Handle data */
    private TFTPPacket onData(TFTPPacket pkt, TFTPSession ses) throws Exception {
        return TFTPPacket.ERROR(TFTPPacket.ERROR_ACCESS_VIOLATION, "Writing is not implemented on this server");
    }

    /* Handle a data acknowledge */
    private TFTPPacket onAcknowledge(TFTPPacket pkt, TFTPSession ses) throws Exception {
        TFTPPacket rep = TFTPPacket.DATA((char) (pkt.block_id + 1), null);
        ses.readFile(rep);
        return rep;
    }

    /* Handles received packets */
    @Override
    protected boolean OnReceive(InetSocketAddress sender, int remotePort, ByteBuffer data) throws Exception {
        /* Get client ip address */
        int addr = IP.BytesToInt(sender.getAddress().getAddress());
        /* Get session */
        TFTPSession ses = TFTPSession.Manager.FindOrStart(addr, remotePort);
        /* Parse packet */
        TFTPPacket pkt = new TFTPPacket(data);
        /* Log received packet if configuration specifies to */
        if ((pkt.op != TFTPPacket.OP_DATA && pkt.op != TFTPPacket.OP_ACK) && TFTPConfig.tftp_log_packets)
            Log.i(TAG, "TFTP " + TFTPPacket.GetOpText(pkt.op) + " received from " + IP.IntToStr(addr));
        /* Generate a reply */
        ses.heartbeat();
        TFTPPacket rep = null;
        try {
            switch (pkt.op) {
                case TFTPPacket.OP_RRQ:
                    rep = onReadRequest(pkt, ses);
                    break;

                case TFTPPacket.OP_WRQ:
                    rep = onWriteRequest(pkt, ses);
                    break;
                case TFTPPacket.OP_OACK:
                    rep = TFTPPacket.ACK((char) 0);
                    break;
                case TFTPPacket.OP_DATA:
                    rep = onData(pkt, ses);
                    break;
                case TFTPPacket.OP_ACK:
                    if (!ses.endOfFile())
                        rep = onAcknowledge(pkt, ses);
                    break;
                case TFTPPacket.OP_ERROR:
                default:
                    rep = null;
            }
        }
        /* File not found */ catch (FileNotFoundException e) {
            rep = TFTPPacket.ERROR(TFTPPacket.ERROR_FILE_NOT_FOUND, e.getMessage());
        }
        /* I/O error */ catch (IOException e) {
            rep = TFTPPacket.ERROR(TFTPPacket.ERROR_UNDEFINED, e.getMessage());
        }
        /* Unknown error */ catch (Exception e) {
            rep = TFTPPacket.ERROR(TFTPPacket.ERROR_UNDEFINED, e.getMessage());
        }
        /* Log and send reply */
        if (rep != null) {
            /* Log reply packet */
            if ((rep.op == TFTPPacket.OP_RRQ || rep.op == TFTPPacket.OP_WRQ || rep.op == TFTPPacket.OP_OACK) && TFTPConfig.tftp_log_packets)
                Log.i(TAG, "TFTP " + TFTPPacket.GetOpText(rep.op) + " sent to " + IP.IntToStr(addr));
            if (rep.op == TFTPPacket.OP_ERROR && TFTPConfig.tftp_log_packets)
                Log.i(TAG, "TFTP " + TFTPPacket.GetOpText(rep.op) + " " + rep.ErrorCode + ":\"" + rep.ErrorMessage + "\" sent to " + IP.IntToStr(addr));
            /* Send packet */
            //Log("recieve: " + rep.op);
            Send(sender, remotePort, rep);
        }
        return true;
    }
}
