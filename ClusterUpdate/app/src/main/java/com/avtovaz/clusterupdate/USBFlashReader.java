package com.avtovaz.clusterupdate;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.channels.FileChannel;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public class USBFlashReader {

    public boolean checkUpdateFiles(Context context, String usbPath) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        boolean fileFound = false;

        try {
            File file = new File(usbPath + context.getString(R.string.update_name));
            File file_sig = new File(usbPath + context.getString(R.string.update_signature_name));
            fileFound = file.exists() && file_sig.exists();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileFound;
    }

    public static Boolean copyUpdateFiles(String filename, String dst, AtomicReference<Boolean> cancelled) {
        try {
            File file = new File(Paths.getInstance().getUSB() + filename);
            dst += filename;
            if (file.exists()) {
                exportFile(file, new File(dst), cancelled);
                return true;
            }
            throw new Exception("File " + filename + " doesn't exist");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void exportFile(File src, File dst, AtomicReference<Boolean> cancelled) throws IOException {
        FileChannel inChannel;
        FileChannel outChannel;

        inChannel = new FileInputStream(src).getChannel();

        if (!dst.exists()) {
            File parent = dst.getParentFile();
            if (!Objects.requireNonNull(parent).exists())
                parent.mkdirs();

            dst.createNewFile();
        }
        outChannel = new FileOutputStream(dst).getChannel();

        long pos = 0;
        long size = inChannel.size();

        try {
            assert inChannel != null;
            int blksize = 4096;
            boolean cancelled_local = cancelled.get();

            while (pos <= size && !cancelled_local) {
                inChannel.transferTo(pos, blksize, outChannel);
                pos += blksize;
                cancelled_local = cancelled.get();
            }
        } finally {
            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();
        }
    }
}


