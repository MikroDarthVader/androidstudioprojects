package com.avtovaz.clusterupdate;

import android.app.Application;
import android.content.Context;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import yandex.auto.customizationsdk.CustomizationSdk;

public class UpdateApplication extends Application {

    public static ExecutorService getExecutor() {
        return Executors.newCachedThreadPool();
    }

    private static UpdateApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        CustomizationSdk.init(this, CustomizationSdk.CustomizableApp.COMMON);
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }
}