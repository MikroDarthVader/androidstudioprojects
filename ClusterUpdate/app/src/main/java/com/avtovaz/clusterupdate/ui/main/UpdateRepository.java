package com.avtovaz.clusterupdate.ui.main;

import android.content.Context;

import com.avtovaz.clusterupdate.HashWorker;
import com.avtovaz.clusterupdate.Paths;
import com.avtovaz.clusterupdate.R;
import com.avtovaz.clusterupdate.USBFlashReader;
import com.avtovaz.clusterupdate.ftp.FsService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;


interface UpdateRepositoryCallback<Boolean> {
    void onComplete(Boolean result);
}

abstract class StepWork {
    public static int COMPLETE = 0;
    public static int ERROR = 1;
    public static int CANCELLED = 2;

    public abstract void run(final UpdateRepositoryCallback<Integer> callback);
}

class UpdateStep {
    public final String name;
    public final StepWork work;

    public UpdateStep(String name, StepWork work) {
        this.name = name;
        this.work = work;
    }
}

public class UpdateRepository {

    private ExecutorService executor;
    private final Context context;
    private LinkedList<UpdateStep> updateSteps;
    private AtomicReference<Boolean> cancelled;

    public UpdateRepository(Context context) {
        this.context = context;
        this.cancelled = new AtomicReference<Boolean>(false);

        updateSteps = new LinkedList();
        updateSteps.add(new UpdateStep(context.getString(R.string.clean_data_status), new Init()));
        updateSteps.add(new UpdateStep(context.getString(R.string.copy_status), new CopyUpdateFiles()));
        updateSteps.add(new UpdateStep(context.getString(R.string.verify_status), new VerifyUpdateFiles()));
        updateSteps.add(new UpdateStep(context.getString(R.string.starting_server_status), new StartFTPServer()));
    }

    public void Init(ExecutorService executor) {
        cancelled.set(false);
        if (this.executor != null)
            this.executor.shutdownNow();
        this.executor = executor;
    }

    public void CancelWork() {
        cancelled.set(true);
    }

    public Iterator<UpdateStep> getSteps() {
        return updateSteps.iterator();
    }

    public class Init extends StepWork {
        @Override
        public void run(final UpdateRepositoryCallback<Integer> callback) {
            executor.execute(() -> {
                try {
                    Boolean result = init();
                    callback.onComplete(COMPLETE);
                } catch (Exception e) {
                    callback.onComplete(ERROR);
                }
            });
        }

        public Boolean init() {
            return deleteChildren(new File(Paths.getInstance().getExternal()));
        }

        boolean deleteChildren(File fileOrDirectory) {
            if (cancelled.get())
                return false;

            boolean result = true;

            if (fileOrDirectory.isDirectory())
                for (File child : fileOrDirectory.listFiles()) {
                    result &= deleteChildren(child);
                    result &= child.delete();
                }

            return result;
        }
    }

    public class CopyUpdateFiles extends StepWork {

        @Override
        public void run(final UpdateRepositoryCallback<Integer> callback) {
            executor.execute(() -> {
                Integer result = copyUpdateFilesToTemp();
                callback.onComplete(result);
            });
        }

        public Integer copyUpdateFilesToTemp() {
            try {
                boolean result = USBFlashReader.copyUpdateFiles(context.getString(R.string.update_name), Paths.getInstance().getExternal(), cancelled);
                if (!cancelled.get() && result)
                    result = USBFlashReader.copyUpdateFiles(context.getString(R.string.update_signature_name), Paths.getInstance().getExternal(), cancelled);

                return result ? (cancelled.get() ? CANCELLED : COMPLETE) : ERROR;
            } catch (Throwable e) {
                return ERROR;
            }
        }
    }

    public class VerifyUpdateFiles extends StepWork {

        @Override
        public void run(final UpdateRepositoryCallback<Integer> callback) {
            executor.execute(() -> {
                try {
                    Boolean result = verifyUpdateFiles();
                    callback.onComplete(result ? (cancelled.get() ? CANCELLED : COMPLETE) : ERROR);
                } catch (Exception e) {
                    callback.onComplete(ERROR);
                }
            });
        }

        public boolean verifyUpdateFiles() throws Exception {
            File isoFile = new File(Paths.getInstance().getExternal() + context.getString(R.string.update_name));
            String calculatedHash = new HashWorker().CalculateHash(isoFile, cancelled);

            if (cancelled.get())
                return true;

            String hashFromFile = getStringFromFile(Paths.getInstance().getExternal() +
                    context.getString(R.string.update_signature_name)).replaceAll("[\n\0]", "");

            return calculatedHash.equals(hashFromFile);
        }

        public String convertStreamToString(InputStream is) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            return sb.toString();
        }

        public String getStringFromFile(String filePath) throws Exception {
            File fl = new File(filePath);
            FileInputStream fin = new FileInputStream(fl);
            String ret = convertStreamToString(fin);
            //Make sure you close all streams.
            fin.close();
            return ret;
        }
    }

    public class StartFTPServer extends StepWork {

        @Override
        public void run(final UpdateRepositoryCallback<Integer> callback) {
            executor.execute(() -> {
                try {
                    Integer result = StartTFTP();
                    callback.onComplete(result);
                } catch (Exception e) {
                    callback.onComplete(ERROR);
                }
            });
        }

        public Integer StartTFTP() throws InterruptedException {
       /* TFTPConfig.set("clu0", 6969, Paths.getInstance().getTFTPShare());
        if (!TFTPService.isRunning())
            TFTPService.Start(context);*/

            if (!FsService.isRunning())
                FsService.start();
            return COMPLETE;
        }
    }
}
