package com.avtovaz.clusterupdate.tftp;

import android.util.Log;

public abstract class Server implements Runnable {
    private static final String TAG = Server.class.getSimpleName();

    private boolean starting = false;
    private boolean stopping = false;
    private boolean running = false;

    protected abstract void Run() throws Exception;

    protected abstract void PreRun() throws Exception;

    protected abstract void PostRun() throws Exception;

    private Thread thread = null;

    @Override
    public void run() {
        int stage = 0;
        try {
            try {
                PreRun();
                stage = 1;
                setRunning(true);
                setStarting(false);
                Log.d(TAG, "Started");
                Run();
                Log.d(TAG, "Stopping");
            } catch (Exception e) {
                Log.d(TAG, "Failing");
                if (stage == 0)
                    Log.e(TAG,
                            "Server failed to start!  "
                                    + e.getClass().getName() + ": "
                                    + e.getMessage());
                else if (stage == 1)
                    Log.e(TAG,
                            "Server terminated due to untrapped error!  "
                                    + e.getClass().getName() + ": "
                                    + e.getMessage());
            }
        } finally {
            try {
                stage = 2;
                try {
                    PostRun();
                } catch (Exception e) {
                    Log.e(TAG,
                            "Server failed to close cleanly!  "
                                    + e.getClass().getName() + ": "
                                    + e.getMessage());
                }
            } finally {
                setRunning(false);
                setStarting(false);
                setStopping(false);
            }
            Log.e(TAG, "Stopped");
        }
    }

    public void Start(boolean wait) throws Exception {
        if (getRunning()) {
            Log.e(TAG, "Attempted to start server when it is already running");
            throw new Exception("Cannot start server: server is already running");
        }
        setStarting(true);
        thread = new Thread(this);
        Log.i(TAG, "Starting");
        thread.start();
        if (wait)
            while (getStarting())
                Thread.sleep(50);
    }

    public synchronized void Stop() {
        if (getRunning())
            setStopping(true);
    }

    public synchronized void setStopping(boolean value) {
        stopping = value;
    }

    public synchronized boolean getStopping() {
        return stopping;
    }

    public synchronized void setStarting(boolean value) {
        starting = value;
    }

    public synchronized boolean getStarting() {
        return starting;
    }

    public synchronized void setRunning(boolean value) {
        running = value;
    }

    public synchronized boolean getRunning() {
        return running;
    }
}