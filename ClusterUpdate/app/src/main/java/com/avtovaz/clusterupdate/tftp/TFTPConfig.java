package com.avtovaz.clusterupdate.tftp;

/*
 * Maintains the server configuration in static fields, and contains a
 * Serializer class which can load/save the contents of these fields to a
 * configuration file
 */
public class TFTPConfig {
    public static String network_interface = "clu0";
    //public static String network_address = "192.168.40.1";
    public static char tftp_server_port = 6969;
    public static boolean tftp_log_packets = false;
    public static String tftp_root_folder = "/storage/emulated/0/";

    public static void set(String network_interface, int port, String root) {
        TFTPConfig.network_interface = network_interface;
        tftp_server_port = (char) port;
        tftp_root_folder = root;
    }
}