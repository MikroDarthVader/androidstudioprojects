package com.avtovaz.clusterupdate;

import android.content.Context;

public class Paths {

    private static final Paths INSTANCE = new Paths();

    private String USB;
    private String TFTPShare;
    private String TEMP;
    private String external;

    private Paths() {
    }

    public String getUSB() {
        return USB;
    }

    public void setUSB(String path) {
        USB = path;
    }

    public String getTFTPShare() {
        return TFTPShare;
    }

    public String getTEMP() {
        return TEMP;
    }

    public void setTFTPShare(String TFTPShare) {
        this.TFTPShare = TFTPShare;
    }

    public void setTEMP(String TEMP) {
        this.TEMP = TEMP;
    }

    public void setExternal(String external) {
        this.external = external;
    }

    public String getExternal() {
        return external;
    }

    public static Paths getInstance() {
        return INSTANCE;
    }

    public static void init(Context context, String USBPath) {
        Paths inst = getInstance();
        inst.setExternal(context.getExternalFilesDir(null).getPath() + "/");
        inst.setTFTPShare(inst.getExternal() + context.getString(R.string.TFTP_share_dir));
        inst.setTEMP(inst.getExternal() + context.getString(R.string.temp_dir));
        inst.setUSB(USBPath);
    }
}
