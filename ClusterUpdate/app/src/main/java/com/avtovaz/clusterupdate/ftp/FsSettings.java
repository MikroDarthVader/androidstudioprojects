/*
Copyright 2011-2013 Pieter Pareit
Copyright 2009 David Revell

This file is part of SwiFTP.

SwiFTP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SwiFTP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SwiFTP.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.avtovaz.clusterupdate.ftp;

import com.avtovaz.clusterupdate.Paths;
import com.avtovaz.clusterupdate.ftp.server.FtpUser;

import java.io.File;
import java.io.IOException;

public class FsSettings {

    private static class FSDefaults {
        private final static String username_default = "U1";
        private final static String password_default = "P1";
        private final static String chrootDir_default = Paths.getInstance().getExternal();
        private final static int port_default = 2121;
        private final static boolean allow_anonymous = true;
    }

    private final static String TAG = FsSettings.class.getSimpleName();

    public static FtpUser getUser(String username) throws IOException {
        if (FSDefaults.username_default.equals(username))
            return new FtpUser(FSDefaults.username_default, FSDefaults.password_default, FSDefaults.chrootDir_default);
        return null;
    }

    public static boolean shouldTakeFullWakeLock() {
        return true;
    }

    public static boolean allowAnonymous() {
        return FSDefaults.allow_anonymous;
    }

    public static int getPortNumber() {
        return FSDefaults.port_default;
    }

    public static String getExternalStorageUri() {
        return null;
    }

    public static File getDefaultChrootDir() {
        return new File(FSDefaults.chrootDir_default);
    }
}
