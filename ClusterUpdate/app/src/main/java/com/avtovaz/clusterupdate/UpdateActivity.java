package com.avtovaz.clusterupdate;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.avtovaz.clusterupdate.databinding.UpdateActivityBinding;
import com.avtovaz.clusterupdate.ftp.FsService;
import com.avtovaz.clusterupdate.ui.main.UpdateViewModel;

public class UpdateActivity extends AppCompatActivity {

    private UpdateActivityBinding binding;
    private UpdateViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_activity);
        binding = UpdateActivityBinding.bind(findViewById(R.id.main));
        checkPermission();
        init();
    }

    private void checkPermission() {
        if (getApplicationContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || getApplicationContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
        }
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(UpdateViewModel.class);
        viewModel.getIsError().observe(this, it -> {
            updateUI();
        });

        viewModel.getIsStarted().observe(this, it -> {
            updateUI();
        });

        viewModel.getIsFinished().observe(this, it -> {
            updateUI();
        });

        viewModel.getIsCanceled().observe(this, it -> {
            updateUI();
        });

        viewModel.getCurrentStatus().observe(this, status -> {
            changeStatus(status);
        });

        binding.cancelButton.setOnClickListener(v -> {
            if (viewModel.getIsStarted().getValue() && !viewModel.getIsError().getValue()
                    && !viewModel.getIsCanceled().getValue()) {
                viewModel.cancelUpdate();
            } else if (viewModel.getIsCanceled().getValue()) {
                try {
                    initUIStatuses();
                    viewModel.startUpdate();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                finishAffinity();
                System.exit(1);
            }
        });

        binding.acceptButton.setOnClickListener(v -> {
            if (!viewModel.getIsStarted().getValue() || viewModel.getIsError().getValue()) {
                try {
                    initUIStatuses();
                    viewModel.startUpdate();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                finishAffinity();
                System.exit(1);
            }
        });
    }

    private void initUIStatuses() {
        binding.copyText.setTypeface(null, Typeface.NORMAL);
        binding.verifyText.setTypeface(null, Typeface.NORMAL);
        binding.startingServerText.setTypeface(null, Typeface.NORMAL);

        binding.copyText.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_empty), null, null, null);
        binding.verifyText.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_empty), null, null, null);
        binding.startingServerText.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_empty), null, null, null);

        binding.copyText.setCompoundDrawablePadding(15);
        binding.verifyText.setCompoundDrawablePadding(15);
        binding.startingServerText.setCompoundDrawablePadding(15);

        // binding.copyText.setCompoundDrawableTintMode();
    }

    private void changeStatus(String status) {
        if (status == getString(R.string.clean_data_status)) {
            binding.copyText.setTypeface(null, Typeface.BOLD);
        } else if (status == getString(R.string.copy_status)) {
            binding.copyText.setTypeface(null, Typeface.BOLD);
        } else if (status == getString(R.string.verify_status)) {
            binding.copyText.setTypeface(null, Typeface.NORMAL);
            binding.verifyText.setTypeface(null, Typeface.BOLD);
            binding.copyText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    AppCompatResources.getDrawable(this, R.drawable.ic_done),
                    null,
                    null,
                    null);
        } else if (status == getString(R.string.starting_server_status)) {
            binding.verifyText.setTypeface(null, Typeface.NORMAL);
            binding.startingServerText.setTypeface(null, Typeface.BOLD);
            binding.verifyText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    AppCompatResources.getDrawable(this, R.drawable.ic_done),
                    null,
                    null,
                    null);
        }
    }

    private void updateUI() {

        if (viewModel.getIsError().getValue()) {
            this.setTitle(getString(R.string.title_update_error));
            binding.mainText.setVisibility(View.VISIBLE);
            binding.statusGroup.setVisibility(View.INVISIBLE);
            binding.acceptButton.setVisibility(View.VISIBLE);
            binding.mainText.setText(this.getString(R.string.error_text));
            binding.acceptButton.setButtonText(this.getString(R.string.repeat));
            binding.cancelButton.setButtonText(getString(R.string.cancel));
        } else if (viewModel.getIsCanceled().getValue()) {
            this.setTitle(getString(R.string.title_update_cancel));
            binding.mainText.setText(this.getString(R.string.cancel_text));
            binding.mainText.setVisibility(View.VISIBLE);
            binding.statusGroup.setVisibility(View.INVISIBLE);
            binding.acceptButton.setVisibility(View.VISIBLE);
            binding.acceptButton.setButtonText(getString(R.string.yes));
            binding.cancelButton.setButtonText(getString(R.string.no));
        } else if (viewModel.getIsFinished().getValue()) {
            this.setTitle(getString(R.string.title_update_finished));
            binding.mainText.setVisibility(View.VISIBLE);
            binding.statusGroup.setVisibility(View.INVISIBLE);
            binding.acceptButton.setVisibility(View.INVISIBLE);
            binding.mainText.setText(this.getString(R.string.successful_text));
            binding.cancelButton.setButtonText(getString(R.string.exit));
        } else if (viewModel.getIsStarted().getValue()) {
            this.setTitle(getString(R.string.title_update_loading));
            binding.mainText.setVisibility(View.INVISIBLE);
            binding.acceptButton.setVisibility(View.INVISIBLE);
            binding.statusGroup.setVisibility(View.VISIBLE);
            binding.cancelButton.setButtonText(getString(R.string.cancel));
        } else { // first screen
            this.setTitle(getString(R.string.title_update_founded));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (FsService.isRunning())
            FsService.stop();
    }
}