package com.example.TFTPservice.tftp;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;

/*
 * Maintains the server configuration in static fields, and contains a
 * Serializer class which can load/save the contents of these fields to a
 * configuration file
 */
public class Configuration {
    /* Device to bind to */
    public static String network_interface = "clu0";
    //public static String network_address = "192.168.40.1";
    /* TFTP server */
    public static boolean tftp_enabled = true;
    public static short tftp_server_port = 6969;
    public static boolean tftp_log_packets = false;
    /* TFTP root folder */
    public static String tftp_root_folder = "/storage/emulated/0/";
    /* Serializer for static fields */
}