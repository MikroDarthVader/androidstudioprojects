package com.example.TFTPservice.tftp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.example.TFTPservice.App;

public class TFTPService extends Service {
    private static final String TAG = Server.class.getSimpleName();

    public TFTP getServer() {
        return server;
    }

    static TFTP server;

    // Service will check following actions when started through intent
    static public final String ACTION_REQUEST_START = "com.example.TFTPservice.tftp.REQUEST_START";
    static public final String ACTION_REQUEST_STOP = "com.example.TFTPservice.tftp.REQUEST_STOP";

    // Service will (global) broadcast when server start/stop
    static public final String ACTION_STARTED = "com.example.TFTPservice.tftp.FTPSERVER_STARTED";
    static public final String ACTION_STOPPED = "com.example.TFTPservice.tftp.FTPSERVER_STOPPED";
    static public final String ACTION_FAILEDTOSTART = "com.example.TFTPservice.tftp.FTPSERVER_FAILEDTOSTART";

    public static void Start() {
        Context context = App.getAppContext();
        Intent serviceIntent = new Intent(ACTION_REQUEST_START, null, context, TFTPService.class);
        if (server == null || !server.getRunning()) {
            context.startService(serviceIntent);
        }
    }

    public static void Stop() {
        Context context = App.getAppContext();
        Intent stopReq = new Intent(ACTION_REQUEST_STOP, null, context, TFTPService.class);
        if (server != null && server.getRunning()) {
            context.startService(stopReq);
        }
    }

    public static boolean isRunning() {
        return server != null && server.getRunning();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        switch (intent.getAction()) {
            case ACTION_REQUEST_START:
                try {
                    if (server == null)
                        server = new TFTP();
                    server.Start(true);
                    return START_STICKY;
                } catch (Exception e) {
                    Log.e(TAG, "Cannot start server:: " + e.getClass() + ":" + e.getMessage());
                    return START_NOT_STICKY;
                }
            case ACTION_REQUEST_STOP:
                if (server != null)
                    server.Stop();
                return START_NOT_STICKY;
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        if (server != null)
            server.Stop();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
