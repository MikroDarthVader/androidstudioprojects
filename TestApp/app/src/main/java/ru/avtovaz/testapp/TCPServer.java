package ru.avtovaz.testapp;

import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;

public class TCPServer {
    private final static String TAG = TCPServer.class.getName();
    private final static int timeout = 5000;

    public final int port;
    public final String networkInterface;

    private Socket clientSocket;
    private ServerSocket listenSocket;
    private OutputStream outputStream;

    public TCPServer(String networkInterface, int port) {
        this.port = port;
        this.networkInterface = networkInterface;
    }

    private InetAddress getInterfaceAddress(String ifaceName) {
        InetAddress returnAddress = null;

        try {
            ArrayList<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : networkInterfaces) {
                if (!networkInterface.getName().matches(ifaceName))
                    continue;
                for (InetAddress address : Collections.list(networkInterface.getInetAddresses())) {
                    if (!address.isLoopbackAddress()
                            && !address.isLinkLocalAddress()
                            && address instanceof Inet4Address) {
                        if (returnAddress != null) {
                            Log.w(TAG, "Found more than one valid address local inet address, why???");
                        }
                        returnAddress = address;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "FOUND OWN ADDRESS: " + returnAddress);
        return returnAddress;
    }

    public void acceptClient() throws IOException, InterruptedException {
        Log.d(TAG, "ACCEPTING CLIENT");
        while (clientSocket == null) {
            try {
                if (listenSocket == null) {
                    InetAddress inetAddr = getInterfaceAddress(networkInterface);
                    if (inetAddr == null) {
                        Thread.sleep((long) timeout);
                        continue;
                    }
                    listenSocket = new ServerSocket(port, 1, inetAddr);
                    listenSocket.setReuseAddress(true);
                    listenSocket.setSoTimeout(timeout);
                }

                clientSocket = listenSocket.accept();
                outputStream = clientSocket.getOutputStream();
                Log.d(TAG, "CLIENT FOUND");
            } catch (SocketTimeoutException e) {
                Log.d(TAG, "Waiting for client connection");
            }
        }
    }

    public boolean sendString(String str) {
        try {
            outputStream.write(str.getBytes());
            Log.d(TAG, "Sent to: " + clientSocket + " Data: " + str);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean closeConnection() {
        try {
            if (outputStream != null)
                outputStream.close();
            if (clientSocket != null)
                clientSocket.close();
            listenSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
