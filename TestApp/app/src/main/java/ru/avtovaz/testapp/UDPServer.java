package ru.avtovaz.testapp;

import android.nfc.Tag;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;

public class UDPServer {
    private final String TAG = this.getClass().getName();
    private final static int timeout = 5000;

    public final int port;
    public final String networkInterface;

    private DatagramSocket socket;
    private Thread serverThread;

    private final byte[] clientIP;
    private InetAddress client;

    public UDPServer(String networkInterface, byte[] remoteIP , int port) {
        this.port = port;
        this.networkInterface = networkInterface;
        serverThread = new Thread();
        clientIP = remoteIP;
    }

    public void Init() throws InterruptedException, SocketException {
        while (socket == null || client == null) {
            InetAddress inetAddress = getInterfaceAddress(networkInterface);
            if (inetAddress == null) {
                Thread.sleep((long) timeout);
                continue;
            }

            try {
                client = InetAddress.getByAddress(clientIP);
            }
            catch (UnknownHostException e){
                Thread.sleep((long) timeout);
                continue;
            }
            Log.d(TAG, "CLIENT FOUND");

            socket = new DatagramSocket(port, inetAddress);
            socket.setBroadcast(true);
            socket.setReuseAddress(true);
            socket.setSoTimeout(timeout);
        }
    }

    public boolean sendString(String str) {
        try {
            byte[] pdata = str.getBytes();
            socket.send(new DatagramPacket(pdata, pdata.length, client, port));
            //Log.d(TAG, "UDP Sent to: " + client + " Data: " + str);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private InetAddress getInterfaceAddress(String ifaceName) {
        InetAddress returnAddress = null;

        try {
            ArrayList<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : networkInterfaces) {
                if (!networkInterface.getName().matches(ifaceName))
                    continue;
                for (InetAddress address : Collections.list(networkInterface.getInetAddresses())) {
                    if (!address.isLoopbackAddress()
                            && !address.isLinkLocalAddress()
                            && address instanceof Inet4Address) {
                        if (returnAddress != null) {
                            Log.w(TAG, "Found more than one valid address local inet address, why???");
                        }
                        returnAddress = address;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(returnAddress != null)
            Log.d(TAG, "FOUND OWN ADDRESS: " + returnAddress);
        else
            Log.d(TAG, "OWN ADDRESS NOT FOUND");

        return returnAddress;
    }
}
