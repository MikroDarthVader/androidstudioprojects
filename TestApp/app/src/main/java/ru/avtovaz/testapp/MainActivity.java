package ru.avtovaz.testapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = MainActivity.class.getName();

    //TCPServer testServer;
    //boolean waitingForClient = false;
    static boolean initiated = false;
    UDPServer udpTest;

    /*Thread clientSearcher = new Thread(() -> {
        try {
            waitingForClient = true;
            testServer.acceptClient();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            waitingForClient = false;
        }
    });*/

    Thread th = new Thread(() -> {
        /*while (true) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (waitingForClient)
                continue;
            if (!testServer.sendString("hello client")) {
                testServer.closeConnection();
                clientSearcher.start();
            }
        }*/
        try {
            udpTest.Init();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; true; i++) {
            udpTest.sendString("yodp" + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*if (initiated)
            return;
        udpTest = new UDPServer("clu0", new byte[]{(byte) 192, (byte) 168, 40, 2}, 5001);
        th.start();
        initiated = true;*/
    }

    public void SendHelloButtonPressed(View view) {
        Thread th = new Thread(() -> {
            while (true) {
                SyncTime st = new SyncTime(Instant.now(), TimeZone.getDefault());
                Log.d("Stream Test", st.toString());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        th.start();

        /*Thread th = new Thread(() -> {
            testServer.sendString("hello client");
            try {
                ServerSocket listenSocket = new ServerSocket();
                listenSocket.setReuseAddress(true);
                InetSocketAddress address = new InetSocketAddress(InetAddress.getByAddress(new byte[]{(byte)192, (byte)168, 40, 1}), 5001);
                listenSocket.bind(address);
                Socket clientSocket = listenSocket.accept();
                OutputStream outputStream = clientSocket.getOutputStream();
                outputStream.write("Hellooooooooooooooooo".getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        th.start();*/
    }

    public class SyncTime {
        Instant utcInst;
        TimeZone zone;

        public SyncTime(Instant utc, TimeZone zone) {
            this.utcInst = utc;
            this.zone = zone;
        }

        @NonNull
        @Override
        public String toString() {
            ZonedDateTime utcTime = utcInst.atZone(ZoneOffset.UTC);
            int h = utcTime.getHour();
            int m = utcTime.getMinute();
            int s = utcTime.getSecond();
            int offsetMS = zone.getRawOffset();
            int offsetM = (Math.abs(offsetMS) / 60000) % 60;
            int offsetH = offsetMS / 3600000;

            return String.format("%02d%02d%02d%+03d%02d", h, m, s, offsetH, offsetM);
        }
    }
}